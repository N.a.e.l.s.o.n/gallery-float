<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
          integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <title>Captcha</title>
    <link rel="stylesheet" type="text/css" href="style.css"/>
</head>
<body>

<?php $max = 4 ?>


<div class="content">

    <div class="gallery-title">
        <div class="border-tag-left2"><p class="text-center">Gallery</p></div>
    </div>

        <div class="gallery">
            <ul>
              <?php for ($i = 0; $i < $max;) { $i++?>
                  <li>
                      <div><img width="100"  height="100" id="<?=$i?>gry-id" src="image/i<?=$i?>.jpg"></div>
                  </li>
              <?php } ?>
            </ul>
        </div>


    <div class="gallery-float">

        <div class="image-float">
            <img src="">
            <div class="gallery-option">
                <ul>
                    <li class="gallery-next" onclick="next()"><i class="fas fa-forward"></i></li>
                    <li class="gallery-close"><i class="fas fa-times"></i></li>
                </ul>
            </div>

        </div>

    </div>
</div>


<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
<script type="text/javascript" src="global.js"></script>
</body>
</html>